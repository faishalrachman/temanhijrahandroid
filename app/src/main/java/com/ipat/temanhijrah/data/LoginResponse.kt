package com.ipat.temanhijrah.data

data class LoginResponse(
	val result: LoginData? = null,
	val statusCode: Int? = null,
	val status: String? = null
)
