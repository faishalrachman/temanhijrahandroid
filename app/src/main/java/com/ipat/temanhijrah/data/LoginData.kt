package com.ipat.temanhijrah.data

data class LoginData(
	val birthday: String? = null,
	val firstName: String? = null,
	val lastName: String? = null,
	val image: String? = null,
	val password: String? = null,
	val gender: String? = null,
	val phone: String? = null,
	val _id: String? = null,
	val email: String? = null,
	val username: String? = null
)
