package com.ipat.temanhijrah

class Constants {

    companion object {
        //URLS
        val baseURL = "http://10.30.40.29:8000/api/v1/"
        val prefName = "TemanHijrah"

        //STATUSES
        val SUCCESS = 200
        val NOT_FOUND = 404
    }
}