package com.ipat.temanhijrah.ui.activity

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import com.ipat.temanhijrah.R
import com.ipat.temanhijrah.base.BaseActivity
import com.ipat.temanhijrah.ui.fragment.HomeFragment
import kotlinx.android.synthetic.main.activity_dashboard.*
import android.content.Context.LAYOUT_INFLATER_SERVICE
import android.support.v4.content.ContextCompat.getSystemService
import android.view.LayoutInflater
import android.widget.TextView
import android.widget.Toast
import android.widget.ImageButton




class DashboardActivity : BaseActivity() {
    override fun layoutId(): Int = R.layout.activity_dashboard
    val Home = HomeFragment()
    val Player = HomeFragment()
    val Profile = HomeFragment()


    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                supportFragmentManager.beginTransaction().replace(R.id.frame_fragment, Home).commit()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_player -> {

                supportFragmentManager.beginTransaction().replace(R.id.frame_fragment, Home).commit()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_profile -> {

                supportFragmentManager.beginTransaction().replace(R.id.frame_fragment, Home).commit()
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }


    override fun initData() {

    }

    override fun initView() {
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        navigation.selectedItemId = R.id.navigation_home

    }

    override fun start() {

    }

    override fun initListener() {

    }

}
