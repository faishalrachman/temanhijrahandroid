package com.ipat.temanhijrah.ui.activity

import android.content.Intent
import android.os.Bundle
import com.ipat.temanhijrah.R
import com.ipat.temanhijrah.base.BaseActivity
import kotlinx.android.synthetic.main.activity_main.*

class SplashActivity : BaseActivity() {
    override fun layoutId(): Int = R.layout.activity_main
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun initData() {
        if (pref.is_logged()){
            startActivity(Intent(applicationContext,DashboardActivity::class.java))
            finish()
        }
    }

    override fun initView() {
    }

    override fun start() {
    }

    override fun initListener() {
        btn_login.setOnClickListener {
            val intent = Intent(applicationContext, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }

        btn_register.setOnClickListener {
            val intent = Intent(applicationContext, RegisterActivity::class.java)
            startActivity(intent)
        }
    }

}
