package com.ipat.temanhijrah.ui.activity

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.widget.Toast
import com.ipat.temanhijrah.R
import com.ipat.temanhijrah.base.BaseActivity
import com.ipat.temanhijrah.data.LoginData
import com.ipat.temanhijrah.mvp.contract.RegisterContract
import com.ipat.temanhijrah.mvp.presenter.RegisterPresenter
import kotlinx.android.synthetic.main.activity_register.*
import java.text.SimpleDateFormat
import java.util.*

class RegisterActivity : BaseActivity(),RegisterContract.View{

    val presenter = RegisterPresenter(this)
    override fun doRegister(data: LoginData) {
        Log.d("Login", "Berhasil")
        pref.saveLogin(data._id!!, data.firstName + " " + data.lastName)
        startActivity(Intent(this, DashboardActivity::class.java))
        finish()
    }

    override fun showError(error: String) {
        Toast.makeText(applicationContext, error, Toast.LENGTH_LONG).show()
    }

    override fun showLoading() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun hideLoading() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    lateinit var cal: Calendar

    override fun layoutId(): Int = R.layout.activity_register

    override fun initData() {
    }

    override fun initView() {
        cal = Calendar.getInstance()

        et_tanggal.inputType = InputType.TYPE_NULL
    }

    override fun start() {

    }
    fun validateForm() : Boolean{
        if (et_username.text.isNullOrEmpty() || et_first_name.text.isNullOrEmpty() || et_last_name.text.isNullOrEmpty() || et_email.text.isNullOrEmpty() || et_password.text.isNullOrEmpty() || et_phone.text.isNullOrEmpty()){
            Toast.makeText(applicationContext,"Form masih ada yang kosong",Toast.LENGTH_SHORT).show()
            return false
        }
        if (!et_password.text.equals(et_confirm_password.text)){
            Toast.makeText(applicationContext,"Password tidak sama",Toast.LENGTH_SHORT).show()
            return false
        }
        return true
    }

    override fun initListener() {
        val year = cal.get(Calendar.YEAR)
        val month = cal.get(Calendar.MONTH)
        val day = cal.get(Calendar.DAY_OF_MONTH)
        et_tanggal.setOnClickListener {
            val dpd = DatePickerDialog(this,DatePickerDialog.OnDateSetListener{it,mYear,mMonth,mDay -> et_tanggal.setText(""+mDay+"/"+mMonth+"/"+mYear)},year,month,day)
            dpd.show()
        }
        btn_submit.setOnClickListener {
            if (validateForm()){
                val username = et_username.text.toString()
                val firstName = et_first_name.text.toString()
                val lastName = et_last_name.text.toString()
                val birthday = et_tanggal.text.toString()
                var gender = "Laki-laki"
                if (rb_female.isSelected){
                    gender = "Perempuan"
                }
                val email = et_email.text.toString()
                val phone = et_phone.text.toString()
                val password = et_password.text.toString()
                presenter.registerUser(username, firstName, lastName, birthday, gender, email, phone, password)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
    }

    private fun updateDateInView() {
        val myFormat = "MM/dd/yyyy" // mention the format you need
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        et_tanggal!!.setText(sdf.format(cal.getTime()))
    }
}
