package com.ipat.temanhijrah.ui.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.ipat.temanhijrah.R
import com.ipat.temanhijrah.base.BaseActivity
import com.ipat.temanhijrah.data.LoginData
import com.ipat.temanhijrah.mvp.contract.LoginContract
import com.ipat.temanhijrah.mvp.presenter.LoginPresenter
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseActivity(), LoginContract.View {
    val presenter = LoginPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun doLogin(data: LoginData) {

        Log.d("Login", "Berhasil")
        pref.saveLogin(data._id!!, data.firstName + " " + data.lastName)
        startActivity(Intent(this, DashboardActivity::class.java))
        finish()
    }

    override fun showLoading() {
        loading_spinner.visibility = View.VISIBLE
        btn_login.visibility = View.GONE
    }

    override fun hideLoading() {
        loading_spinner.visibility = View.GONE
        btn_login.visibility = View.VISIBLE
    }

    override fun showError(error: String) {
        Log.d("Login", "Gagal")
        tx_wrongpass.visibility = View.VISIBLE
        Toast.makeText(applicationContext, error, Toast.LENGTH_LONG).show()
    }

    override fun layoutId(): Int = R.layout.activity_login
    override fun initListener() {
        btn_back.setOnClickListener({
            finish()
        })
        tx_forgot.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                val intent = Intent(applicationContext, ForgotActivity::class.java)
                startActivity(intent)
            }
        })
        btn_login.setOnClickListener {

            Log.d("Login", "Ditekan")
            val username = et_username.text.toString().trim()
            val password = et_password.text.toString().trim()
            presenter.checkLogin(username, password)
        }
    }

    override fun initData() {

    }

    override fun initView() {
    }

    override fun start() {

    }
}
