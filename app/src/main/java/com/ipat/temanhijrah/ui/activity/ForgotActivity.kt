package com.ipat.temanhijrah.ui.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.ipat.temanhijrah.R
import kotlinx.android.synthetic.main.activity_forgot.*

class ForgotActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot)
        btn_back.setOnClickListener{
            finish()
        }
        btn_reset.setOnClickListener{
            layout_berhasil.visibility = View.VISIBLE
            layout_isi.visibility = View.GONE
        }
    }
}
