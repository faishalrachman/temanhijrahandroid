package com.ipat.temanhijrah.mvp.presenter

import android.util.Log
import com.google.gson.Gson
import com.google.gson.TypeAdapter
import com.ipat.temanhijrah.Constants
import com.ipat.temanhijrah.data.LoginData
import com.ipat.temanhijrah.data.LoginResponse
import com.ipat.temanhijrah.mvp.contract.LoginContract
import com.ipat.temanhijrah.mvp.model.LoginModel
import org.json.JSONObject
import retrofit.Callback
import retrofit.Response
import retrofit.Retrofit
import java.io.IOException

class LoginPresenter : LoginContract.Presenter {

    private val model = LoginModel()
    private val view: LoginContract.View

    constructor(view: LoginContract.View) {
        this.view = view
    }

    override fun checkLogin(username: String, password: String) {
        view.showLoading()
        val call = model.checkLogin(username, password)
        call.enqueue(object : Callback<LoginResponse> {
            override fun onFailure(t: Throwable?) {
                view.hideLoading()
                view.showError("Error = "+t?.message)
            }

            override fun onResponse(response: Response<LoginResponse>?, retrofit: Retrofit?) {
                Log.d("Login", "Code = " + response?.code().toString())
                val status = response?.raw()?.code()
                if (status != Constants.SUCCESS) {
                    view.showError("Login gagal! Username atau password salah")
                } else {
                    val result: LoginData? = response.body()?.result
                    view.doLogin(result!!)
                }
                view.hideLoading()
            }
        })
    }

}