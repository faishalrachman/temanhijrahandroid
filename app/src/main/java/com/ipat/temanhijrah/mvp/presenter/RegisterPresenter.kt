package com.ipat.temanhijrah.mvp.presenter

import android.util.Log
import com.google.gson.Gson
import com.google.gson.TypeAdapter
import com.ipat.temanhijrah.Constants
import com.ipat.temanhijrah.data.LoginData
import com.ipat.temanhijrah.data.LoginResponse
import com.ipat.temanhijrah.mvp.contract.RegisterContract

import com.ipat.temanhijrah.mvp.model.LoginModel
import com.ipat.temanhijrah.mvp.model.RegisterModel
import org.json.JSONObject
import retrofit.Callback
import retrofit.Response
import retrofit.Retrofit
import java.io.IOException

class RegisterPresenter : RegisterContract.Presenter {

    private val model = RegisterModel()
    private val view: RegisterContract.View

    constructor(view: RegisterContract.View) {
        this.view = view
    }

    override fun registerUser(
        username: String,
        firstName: String,
        lastName: String,
        birthday: String,
        gender: String,
        email: String,
        phone: String,
        password: String
    ) {
        view.showLoading()
        val call = model.doRegister(username, firstName, lastName, birthday, gender, email, phone, password)
        call.enqueue(object : Callback<LoginResponse> {
            override fun onFailure(t: Throwable?) {
            }
            override fun onResponse(response: Response<LoginResponse>?, retrofit: Retrofit?) {
                Log.d("Login", "Code = " + response?.code().toString())
                val status = response?.raw()?.code()
                if (status != Constants.SUCCESS) {
                    view.showError("Gagal mendaftar, silahkan periksa kembali data")
                } else {
                    val result: LoginData? = response.body()?.result
                    view.doRegister(result!!)
                }
                view.hideLoading()
            }
        })
    }


}