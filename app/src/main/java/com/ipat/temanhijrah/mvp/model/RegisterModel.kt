package com.ipat.temanhijrah.mvp.model

import com.ipat.temanhijrah.base.BaseModel
import com.ipat.temanhijrah.data.LoginResponse
import retrofit.Call

class RegisterModel : BaseModel() {
    fun doRegister(username: String, firstName : String, lastName : String, birthday : String, gender : String, email : String, phone : String, password :  String): Call<LoginResponse> {
        return apiService.doRegister(username, firstName, lastName, birthday, gender, email, phone, password)
    }
}