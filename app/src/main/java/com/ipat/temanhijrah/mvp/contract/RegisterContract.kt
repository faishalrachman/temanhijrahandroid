package com.ipat.temanhijrah.mvp.contract

import com.ipat.temanhijrah.base.IBaseView
import com.ipat.temanhijrah.data.LoginData

interface RegisterContract {

    interface View : IBaseView {
        fun doRegister(data: LoginData)
        fun showError(error: String)
    }

    interface Presenter {
        fun registerUser(
            username: String,
            firstName: String,
            lastName: String,
            birthday: String,
            gender: String,
            email: String,
            phone: String,
            password: String
        )
    }
}