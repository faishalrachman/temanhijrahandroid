package com.ipat.temanhijrah.mvp.model

import com.ipat.temanhijrah.base.BaseModel
import com.ipat.temanhijrah.data.LoginResponse
import retrofit.Call

class LoginModel : BaseModel() {
    fun checkLogin(username: String, password: String): Call<LoginResponse> {
        return apiService.doLogin(username, password)
    }
}