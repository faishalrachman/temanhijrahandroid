package com.ipat.temanhijrah.mvp.contract

import com.ipat.temanhijrah.base.IBaseView
import com.ipat.temanhijrah.data.LoginData

interface LoginContract {

    interface View : IBaseView  {
        fun doLogin(data : LoginData)
        fun showError(error : String)
    }

    interface Presenter {
        fun checkLogin(username: String, password : String)
    }
}