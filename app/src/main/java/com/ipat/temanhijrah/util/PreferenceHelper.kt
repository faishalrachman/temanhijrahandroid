package com.ipat.temanhijrah.util

import android.content.Context
import android.content.SharedPreferences
import com.ipat.temanhijrah.Constants

class PreferenceHelper(context : Context) {

    private var sharedPref: SharedPreferences = context.getSharedPreferences(Constants.prefName, Context.MODE_PRIVATE)
    private var editor: SharedPreferences.Editor = sharedPref.edit()

    init {
        editor.apply()
    }
    fun saveLogin(id : String, name : String){
        editor.putString("userId", id)
        editor.putString("name", name)
        editor.putBoolean("logged_in", true)
        editor.commit()
    }
    fun logout(){
        editor.remove("userId")
        editor.remove("name")
        editor.remove("logged_in")
        editor.commit()
    }
    fun is_logged():Boolean{
        return sharedPref.getBoolean("logged_in",false)
    }

}