package com.ipat.temanhijrah.base

import com.ipat.temanhijrah.api.ApiServiceInterface

abstract class BaseModel{
    val apiService = ApiServiceInterface.create()

}