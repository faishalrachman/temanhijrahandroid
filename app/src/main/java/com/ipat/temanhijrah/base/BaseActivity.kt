package com.ipat.temanhijrah.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import com.ipat.temanhijrah.R
import com.ipat.temanhijrah.util.PreferenceHelper

abstract class BaseActivity: AppCompatActivity() {

    lateinit var pref : PreferenceHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        pref = PreferenceHelper(applicationContext)
        setContentView(layoutId())
        initData()
        initView()
        start()
        initListener()
    }

    /**
     *  加载布局
     */
    abstract fun layoutId(): Int

    /**
     * 初始化数据
     */
    abstract fun initData()

    /**
     * 初始化 View
     */
    abstract fun initView()

    /**
     * 开始请求
     */
    abstract fun start()
    abstract fun initListener()

}