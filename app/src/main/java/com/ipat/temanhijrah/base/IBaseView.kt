package com.ipat.temanhijrah.base

interface IBaseView{
    fun showLoading()
    fun hideLoading()

}