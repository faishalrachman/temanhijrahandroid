package com.ipat.temanhijrah.api

import com.google.gson.GsonBuilder
import com.ipat.temanhijrah.Constants
import com.ipat.temanhijrah.data.LoginResponse
import retrofit.Call
import retrofit.GsonConverterFactory
import retrofit.Retrofit
import retrofit.RxJavaCallAdapterFactory
import retrofit.http.*
import retrofit.http.POST
import retrofit.http.FormUrlEncoded


interface ApiServiceInterface {


    @FormUrlEncoded
    @POST("login")
    fun doLogin(@Field("username") username: String, @Field("password") password: String): Call<LoginResponse>

    @FormUrlEncoded
    @POST("register")
    fun doRegister(
        @Field("username") username: String, @Field("firstName") firstName: String, @Field("lastName") lastName: String, @Field(
            "birthday"
        ) birthday: String, @Field("gender") gender: String, @Field("email") email: String, @Field("phone") phone: String, @Field(
            "password"
        ) password: String
    ): Call<LoginResponse>

    companion object Factory {
        fun create(): ApiServiceInterface {
            //initialized gson
            val gson = GsonBuilder().create()
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(Constants.baseURL)
                .build()

            return retrofit.create(ApiServiceInterface::class.java)
        }
    }
}